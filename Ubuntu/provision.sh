#!/bin/bash

# Install required packages
sudo apt-get update
sudo apt-get install -y dnsmasq syslinux pxelinux nfs-kernel-server debootstrap apt-cacher-ng tgt

# Detect the IP address of the PXE server
PXE_SERVER_IP=$(ip addr show enp0s8 | awk '/inet / {print $2}' | cut -d'/' -f1)
echo "PXE Server IP: $PXE_SERVER_IP"  # Print the PXE server IP for verification

# Configure dnsmasq
echo "interface=enp0s8" | sudo tee /etc/dnsmasq.conf
echo "dhcp-range=192.168.56.100,192.168.56.254,255.255.255.0,12h" | sudo tee -a /etc/dnsmasq.conf
echo "dhcp-boot=pxelinux.0" | sudo tee -a /etc/dnsmasq.conf
echo "enable-tftp" | sudo tee -a /etc/dnsmasq.conf
echo "tftp-root=/var/lib/tftpboot" | sudo tee -a /etc/dnsmasq.conf

# Create TFTP directory and download netboot loader
sudo mkdir -p /var/lib/tftpboot
sudo cp /usr/lib/PXELINUX/pxelinux.0 /var/lib/tftpboot
sudo cp -r /usr/lib/syslinux/modules/bios/ldlinux.c32 /var/lib/tftpboot

# Create PXE configuration directory
sudo mkdir -p /var/lib/tftpboot/pxelinux.cfg

# Download installer for iSCSi
sudo wget -O /var/lib/tftpboot/linux http://us.archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/ubuntu-installer/amd64/linux
sudo wget -O /var/lib/tftpboot/initrd.gz http://us.archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/ubuntu-installer/amd64/initrd.gz

# Download kernel and initrd to TFTP directory
sudo cp /boot/vmlinuz-4.15.0-210-generic /var/lib/tftpboot/
sudo cp /boot/initrd.img-4.15.0-210-generic /var/lib/tftpboot/

# Set ownership and permissions for TFTP files
sudo chown -R nobody:nogroup /var/lib/tftpboot
sudo chmod -R 755 /var/lib/tftpboot

# Configure apt-cacher-ng as a local package repository
sudo sed -i 's/# ForeGround: 0/ForeGround: 1/' /etc/apt-cacher-ng/acng.conf
sudo systemctl restart apt-cacher-ng


#iscsi

# Create a 20 GB virtual disk drive at /etc/tgt/target1
sudo dd if=/dev/zero of=/etc/tgt/target1 bs=1M count=20480  # 20 GB = 20 * 1024 MB


# Create an iSCSI target configuration
sudo tgtadm --lld iscsi --op new --mode target --tid 1 -T iqn.2023-09.com.example:target1
sudo tgtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 1 -b /etc/tgt/target1
sudo tgtadm --lld iscsi --op bind --mode target --tid 1 -I ALL

# Check the target
# sudo tgtadm --lld iscsi --op show --mode target

# Target configuration
sudo mkdir -p /etc/tgt/conf.d/target iqn.2023-09.com.example:target1.conf
echo '<target iqn.2023-09.com.example:target1>
  backing-store /etc/tgt/target1
  incominguser a 1234
  outgoinguser a 1234
  acl target iqn.2023-09.com.example:target1
</target>' | sudo tee -a /etc/tgt/conf.d/target iqn.2023-09.com.example:target1.conf

# Configure `tgt` using the configuration file in /etc/tgt
sudo cp /etc/tgt/targets.conf /etc/tgt/targets.conf.bak  # Backup the original configuration file
# Customize targets.conf if necessary (e.g., add target and initiator ACLs)

# Restart the `tgt` service to apply changes
sudo systemctl restart tgt



# Create NFS root filesystems for each client
CLIENTS=("client1" "client2")  # Add more client names as needed

for CLIENT in "${CLIENTS[@]}"; do
  NFS_ROOT_DIR="/nfsroots/$CLIENT"
  sudo mkdir -p "$NFS_ROOT_DIR"
  
  # Perform debootstrap for each client
  if ! command -v debootstrap &> /dev/null; then
    sudo apt-get install -y debootstrap
  fi

  sudo debootstrap --arch=amd64 bionic "$NFS_ROOT_DIR" http://archive.ubuntu.com/ubuntu

  # Mount necessary filesystems for chroot
  sudo mount -o bind /dev "$NFS_ROOT_DIR/dev"
  sudo mount -o bind /proc "$NFS_ROOT_DIR/proc"
  sudo mount -o bind /sys "$NFS_ROOT_DIR/sys"

  # Customize client-specific configurations (example: hostname and network)
  echo "$CLIENT" | sudo tee "$NFS_ROOT_DIR/etc/hostname"
  echo "auto lo" | sudo tee -a "$NFS_ROOT_DIR/etc/network/interfaces"
  echo "iface lo inet loopback" | sudo tee -a "$NFS_ROOT_DIR/etc/network/interfaces"

  # Configure APT to use the local package repository
  echo "deb http://$PXE_SERVER_IP:3142/archive.ubuntu.com/ubuntu bionic main restricted universe multiverse" | sudo tee "$NFS_ROOT_DIR/etc/apt/sources.list"
  echo "deb-src http://$PXE_SERVER_IP:3142/archive.ubuntu.com/ubuntu bionic main restricted universe multiverse" | sudo tee -a "$NFS_ROOT_DIR/etc/apt/sources.list"


  # Avoid running package configuration scripts during chroot
  sudo mv "$NFS_ROOT_DIR/etc/kernel/postinst.d/dkms" "$NFS_ROOT_DIR/etc/kernel/postinst.d/dkms.disabled"
  sudo mv "$NFS_ROOT_DIR/etc/kernel/postinst.d/initramfs-tools" "$NFS_ROOT_DIR/etc/kernel/postinst.d/initramfs-tools.disabled"
  
  # Chroot into the new root filesystem and make user and password changes
  sudo chroot "$NFS_ROOT_DIR" /bin/bash -c "apt-get update; export DEBIAN_FRONTEND=noninteractive; apt-get install -y --no-install-recommends init; dpkg --configure -a; echo 'root:vagrant' | chpasswd; groupadd vagrant && useradd -m -g vagrant -p '*' vagrant && echo -e 'vagrant\nvagrant' | passwd vagrant; exit"

  # Unmount the filesystems
  sudo umount "$NFS_ROOT_DIR/dev"
  sudo umount "$NFS_ROOT_DIR/proc"
  sudo umount "$NFS_ROOT_DIR/sys"

  # Set ownership and permissions for NFS files
  sudo chown -R nobody:nogroup "$NFS_ROOT_DIR"
  sudo chmod -R 755 "$NFS_ROOT_DIR"

  # Export the NFS directory
  echo "$NFS_ROOT_DIR *(rw,sync,no_subtree_check,no_root_squash,insecure)" | sudo tee -a /etc/exports
done

# Create a single PXE configuration file with multiple menu entries
sudo tee /var/lib/tftpboot/pxelinux.cfg/default > /dev/null <<EOF
DEFAULT menu
PROMPT 0
MENU TITLE PXE Boot Menu
TIMEOUT 200

LABEL client1
MENU LABEL Client 1
KERNEL vmlinuz-4.15.0-210-generic
APPEND vga=normal initrd=initrd.img-4.15.0-210-generic ip=dhcp selinux=0 netdev=enp0s8 root=/dev/nfs nfsroot=$PXE_SERVER_IP:/nfsroots/client1 rw

LABEL client2
MENU LABEL Client 2
KERNEL vmlinuz-4.15.0-210-generic
APPEND vga=normal initrd=initrd.img-4.15.0-210-generic ip=dhcp selinux=0 netdev=enp0s8 root=/dev/nfs nfsroot=$PXE_SERVER_IP:/nfsroots/client2 rw

LABEL install
MENU LABEL Install Ubuntu
KERNEL linux
APPEND vga=normal initrd=initrd.gz ip=dhcp netdev=enp0s8 

LABEL iscsi
KERNEL vmlinuz-4.15.0-210-generic
APPEND initrd=initrd.img-4.15.0-210-generic iscsi_initiator=iqn.2023-09.com.example:initiator1 ip=dhcp iscsi_target_name=iqn.2023-09.com.example:target1 iscsi_target_ip=192.168.56.3 iscsi_target_port=3260 iscsi_target_iqn=iqn.2023-09.com.example:target1 root=/dev/sdc1 rw
EOF

# Restart services
sudo systemctl restart dnsmasq
sudo systemctl restart nfs-kernel-server



