# group_05_project
## Name
High Frequency Trading Network Booting and Latency Analysis

## Members

Hyoungwoo Hahm(hwhahm2@illinois.edu)

Manan Jain(mananj2@illinois.edu)

Nihal Saxena (nihals3@illinois.edu)

Arul Verma (arulhv2@illinois.edu)

## Final Submission Report
[Final Report](https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_spring_2023/ie421_hft_spring_2023_group_05/group_05_project/-/blob/main/FinalReport.md?ref_type=heads)