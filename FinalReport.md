# **Scope**

## **Original Objective:**
We initially aimed to construct a versatile network bootloader capable of remotely booting Linux VMs and potentially actual Linux PCs, presuming these systems were network-connected. The subsequent objectives were to:

1. Facilitate communication between the boot-up entities.
2. Capture the inter-device traffic.
3. Optimize the latency of said communications across clients.

## **Scope Modifications (September):**
As the Fall 2023 semester began, we and Professor Lariviere acknowledged the constraints of our timeline. Consequently, we augmented our scope to include the following comprehensive objectives, moving away from optimizing latency across clients:

1. **Multiple Operating Systems Integration:**
    - Support for multiple OSes and their respective versions including Fedora, Ubuntu, and RHEL.
2. **Diverse Booting Options, Including Pure Disk-less, NFS-less RAM Drives:**
    - NFS-backed systems.
    - Full RAM drive.
    - iSCSI systems.
3. **PXE Server Configurations:**
    - The PXE server should double as a local package repository. This ensures that both the PXE server and any attached physical computer can remain offline, yet still perform package operations using apt, dnf, or yum.
    - The PXE server should also serve as an HTTPS local proxy with capabilities to cache frequently downloaded files.
    - Cache any git pull requests to a PXE server-hosted set of git repositories.
4. **Networking:**
    - Implement DNS configurations for all interconnected systems.
5. **Optimizations:**
    - Employ Packer to generate the base VM from scratch.
    - Minimize the size of the base image to achieve the smallest possible footprint.

## **Overall Intent:**
Our adjusted trajectory necessitates delivering an optimized, comprehensive network booting and communication system. Despite the shift in goals, the underlying aim remains consistent: achieve efficient latency optimizations across all VMs.

# Achievements and Scope Realization

1. **Ubuntu Distribution:**
    - We successfully initiated the boot-up process on an Ubuntu distribution.
    - We added iSCSI functionality: the PXE server can currently create an iSCSI target, allowing clients, during the Ubuntu installation via PXE, to install the OS on the iSCSI disk.
    - We created two NFS root directories that can boot up two separate clients. This process is generalizable and can boot up any N number of VM's but our local machines can only support up to two.
2. **Fedora Distribution:**
    - While Professor Lariviere provided us with a boot up script for Fedora VMs, we successfully built upon this to support multiple clients and monitor the latency using sfnettest.
    - We also integrated a RAM-based system to boot up Fedora VMs. We built a boot up script and designed a full RAM drive system ourselves.
3. **Operating System Integration:**
    - We incorporated Packer functionality that spans across multiple versions of Fedora, RHEL, and Ubuntu distributions.
4. **Networking Enhancements:**
    - We developed a functional internet proxy for client VMs which efficiently caches accessed data, allowed for reduced latency when access data that has already been previously accessed.
    - We developed a local git repository on the pxeserver for client VMs to pull from. This allows client VM's to have access to common git files even without needing connection to the internet.
5. **Unscoped Achievements**
    - A large portion of our team (2/4) had M1 Macs and were not able to run VM's with Vagrant. 1 member of our team had a Mac with an x86 chip which cause some complications as well.
    - We resolved the issues in booting up VM's for computers with M1 (Macs) and wrote up a detailed [ReadMe](https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_spring_2023/ie421_hft_spring_2023_group_05/group_05_project/-/blob/main/VMs_for_M1_tutorial.md?ref_type=heads) on how to set up and boot your VM. 

## **Conclusion:**
Our team effectively addressed the intricacies of the revised project scope, specifically with Ubuntu, Fedora, and RAM drive systems. Our achievements spanned various domains, including the integration of Ubuntu's iSCSI, the development of the RAM drive system, and the adaptation of Packer across multiple versions of OSes. Through our concerted efforts, we managed to navigate challenges, adapt our initial goals, and realize substantive outcomes.