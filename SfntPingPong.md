Instructions for installing Ubuntu(18.04) PXE server and booting up the client through network using NFS and iSCSI. You would need a vagrant and virtual box installed and a Ubuntu 18.04 iso files for the basic box setups. 

**Setting up the Server**
1. cd into the ‘Ubuntu’ folder
2. type ‘vagrant up’ in the terminal to set up a server and to provision it.

**Setting up the Client(NFS)**  
1. Cd into the /Clients/NFS1 to boot up the client.  
2. Once ‘boot :’ appears in the terminal, type ‘client1’ to boot up.(cllient2 will boot up with nfsroot/clients2 nfs root)  
3. After the booting it will require username and password which will be ‘root’ and ‘vagrant’ for the following.  
4. After successfully logging in to the client, run the ‘dsn.sh’ file in the ’Clients’ folder.  

**Running the Sfnt-pingpong script**  
1. After logging into the client follow the instructions In the ‘install.sh’ in the [LatencyScripts/Scripts](https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_spring_2023/ie421_hft_spring_2023_group_05/group_05_project/-/blob/main/LatencyScripts/Scripts/install.sh?ref_type=heads) folder.  
2. Set up a node using ‘./sfnt-pingpong’ command in a vm that you want to connect.  
3. After that boot up and login to the different client and run ‘./sfnt-pingpong tct <ipaddress of the vm on the 2. Step>’ after you also follow the 1. Step for this client vm.  

**Setting up the Client(NFS)**  
1. Cd into the /Clients/iSCSI to boot up the client.  
2. Once ‘boot :’ appears in the terminal, type ‘install’ to boot up.  
3. After installation starts go through the installation steps.  
4. When you reach ‘partitioning disk’ part select ‘manual’ and select ‘Configure iSCSI volumes’.  
5. It will ask for the server ip, initiator iqn, username, and password. Those will be ‘192.168.56.3’, ‘iqn.2023-09.com.example:target1’, ‘a’, ‘1234’ . You can skip the name for the target.  
6. After this select the target and finsish the iSCSI configuration.  
7. Next select ‘sdc’ virtual hard disk and partition it, and then select that partition as the root file system.
Finish the installation.  


