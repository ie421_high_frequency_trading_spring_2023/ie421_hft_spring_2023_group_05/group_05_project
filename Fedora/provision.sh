# !/bin/bash

sudo dnf install dhcp tftp tftp-server syslinux httpd iscsi-initiator-utils targetcli


echo -e "[connection]\n\
id=eth1\n\
uuid=$(uuidgen)\n\
type=ethernet\n\
interface-name=eth1\n\
[ipv4]\n\
method=manual\n\
addresses=192.168.56.3/24\n\
[ipv6]\n\
method=disabled" | sudo tee /etc/NetworkManager/system-connections/eth1.nmconnection

sudo chmod 600 /etc/NetworkManager/system-connections/eth1.nmconnection
sudo systemctl restart NetworkManager      
sudo nmcli con up eth1

#stop and perm disable firewall from starting on reboot
sudo systemctl stop firewalld && systemctl disable firewalld

#install packages
sudo dnf install -y dnsmasq nfs-utils syslinux-tftpboot syslinux dracut-network --disablerepo=* --enablerepo=fedora --enablerepo=updates
sudo dnf install -y debootstrap iscsi-initiator-utils targetcli --disablerepo=* --enablerepo=fedora --enablerepo=updates
echo "PACKAGES INSTALLED"

# Configure dnsmasq
cat <<EOT | sudo tee /etc/dnsmasq.conf
dhcp-range=192.168.56.100,192.168.56.254,255.255.255.0,12h
interface=eth1
enable-tftp
tftp-root=/var/ftpd


dhcp-host=52:54:00:55:66:77,192.168.56.104
dhcp-host=52:54:00:55:66:88,192.168.56.105

log-facility=/var/log/dnsmasq.log
log-queries
log-dhcp

# PXE menu
pxe-prompt="Press F8 for boot menu", 1
pxe-service=x86PC, "Boot from network", pxelinux

# NFS root settings for the client
dhcp-option=17,192.168.56.3:/srv/nfsroot1
EOT
echo "CONFIGURED DNSMASQ"

# CREATE TFTP DIRECTORY
sudo mkdir -p /var/ftpd
sudo chown nobody:nobody /var/ftpd
echo "CREATE TFTP DIRECTORY"

VMLINUZ_FILE=$(ls /boot/vmlinuz-* | grep -v "rescue" | sort -V | tail -n 1)
INITRAMFS_FILE=$(ls /boot/initramfs-*.img | grep -v "rescue" | sort -V | tail -n 1)

# Copy the files to the destination
sudo cp "$VMLINUZ_FILE" /var/ftpd/vmlinuz
sudo cp "$INITRAMFS_FILE" /var/ftpd/initrd.img

#copy pxelinux.0 and other necessary boot files
sudo cp /usr/share/syslinux/pxelinux.0 /var/ftpd/
sudo cp /usr/share/syslinux/ldlinux.c32 /var/ftpd/

dracut --add "nfs network ssh-client ifcfg network-manager usrmount" -f /var/ftpd/initramfs-6.4.14-200.fc38.x86_64.img 6.4.14-200.fc38.x86_64
chmod 644 /var/ftpd/initramfs-6.4.14-200.fc38.x86_64.img
echo "DRACUT INITRAMFS_FILE GENERATED"

sudo wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/ubuntu-installer/amd64/linux -P /var/ftpd/
sudo wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/ubuntu-installer/amd64/initrd.gz -P /var/ftpd/

# Configure Boot menu
sudo mkdir -p /var/ftpd/pxelinux.cfg
sudo chown -R nobody:nobody /var/ftpd/pxelinux.cfg/

cat <<EOT | sudo tee /var/ftpd/pxelinux.cfg/default
DEFAULT menu
PROMPT 0
MENU TITLE PXE Boot Menu
TIMEOUT 200

LABEL client1
MENU LABEL Client 1
KERNEL vmlinuz
APPEND initrd=initramfs-6.4.14-200.fc38.x86_64.img ip=dhcp selinux=0 root=/dev/nfs nfsroot=192.168.56.3:/srv/nfsroot1 rw

LABEL client2
MENU LABEL Client 2
KERNEL linux
APPEND initrd=initramfs-6.4.14-200.fc38.x86_64.img ip=dhcp selinux=0 root=/dev/nfs nfsroot=192.168.56.3:/srv/nfsroot2 rw

LABEL ramboot
MENU LABEL Ramboot
KERNEL vmlinuz
APPEND initrd=initramfs-6.4.14-200.fc38.x86_64.img ip=dhcp selinux=0 root=/dev/ram0 rw
EOT

# Configure NFS
cat <<EOT | sudo tee -a /etc/exports
/srv/nfsroot1 192.168.56.0/24(rw,sync,no_root_squash,no_subtree_check)
/srv/nfsroot2 192.168.56.0/24(rw,sync,no_root_squash,no_subtree_check)
EOT

sudo mkdir -p /srv/nfsroot1
sudo mkdir -p /srv/nfsroot2
echo "NFS CONFIGURED"

# Install fedora38 rootfs
sudo dnf -y --installroot=/srv/nfsroot1 --releasever=38 install @core
echo "FEDORA38 INSTALL 1 COMPLETE"
sudo dnf -y --installroot=/srv/nfsroot2 --releasever=38 install @core
echo "FEDORA38 INSTALL 2 COMPLETE"

sudo chroot /srv/nfsroot1 /bin/bash -c "echo 'root:vagrant' | chpasswd"
sudo chroot /srv/nfsroot1 /bin/bash -c "groupadd vagrant && useradd -m -g vagrant -p '*' vagrant && echo -e 'vagrant\nvagrant' | passwd vagrant"

sudo chroot /srv/nfsroot2 /bin/bash -c "echo 'root:vagrant' | chpasswd"
sudo chroot /srv/nfsroot2 /bin/bash -c "groupadd vagrant && useradd -m -g vagrant -p '*' vagrant && echo -e 'vagrant\nvagrant' | passwd vagrant"

sudo dnf install -y squid
sudo dnf install -y httpd

sudo tee /etc/squid/squid.conf>/dev/null<< EOF
# Squid proxy configuration file

# Port to listen on (3128 is the default)
http_port 3128
http_access allow all

# Allow all clients to use the proxy
acl localnet src 0.0.0.0/0.0.0.0
http_access allow localnet

# Define cache directory and size
cache_dir ufs /var/spool/squid 100 16 256

# Recommended minimum configuration to enable caching
refresh_pattern ^ftp:           1440    20%     10080
refresh_pattern ^gopher:        1440    0%      1440
refresh_pattern -i (/cgi-bin/|\?) 0     0%      0
refresh_pattern (Release|Packages(.gz)*)$      0       20%     2880
refresh_pattern .               0       20%     4320

# Define the hierarchy_stoplist (disable hierarchy for certain sites)
hierarchy_stoplist cgi-bin ?

# Enable DNS lookups (you may also specify a DNS server here)
dns_nameservers 8.8.8.8 8.8.4.4

# Log access to the cache
access_log /var/log/squid/access.log

# Additional options and ACLs can be added as needed

# Example ACL to block specific websites
# acl block_websites dstdomain .example.com
# http_access deny block_websites

# Example ACL to allow only certain IP ranges
# acl trusted_network src 192.168.1.0/24
# http_access allow trusted_network
# http_access deny all
EOF

# Start Services
sudo systemctl start squid
sudo systemctl enable sshd
sudo systemctl start sshd
sudo dnf install -y git gcc pciutils

sudo mkdir -p /var/git-repos
cd /var/git-repos
sudo git clone https://github.com/Xilinx-CNS/cns-sfnettest.git
sudo touch /etc/httpd/conf.d/git-repos.conf
sudo tee /etc/httpd/conf.d/git-repos.conf>/dev/null<< EOF
Alias /git-repos /var/git-repos

<Directory /var/git-repos>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Require all granted
</Directory>
EOF

sudo tee -a /etc/httpd/conf/httpd.conf>/dev/null<< EOF
Listen 443
EOF


sudo systemctl start httpd


# Start Services
sudo systemctl stop systemd-resolved && systemctl disable systemd-resolved
sudo systemctl enable nfs-server --now
sudo systemctl start dnsmasq