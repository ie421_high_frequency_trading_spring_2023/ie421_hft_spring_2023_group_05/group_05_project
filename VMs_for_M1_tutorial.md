# Vagrant and VMWare Fusion 13 Player on M1/M2 devices
This document summarizes notes taken to make VMWare Fusion 13 Player work on Apple M1 Pro.

Sourced from https://gist.github.com/sbailliez/2305d831ebcf56094fd432a8717bed93

*Note: Although I mostly followed this tutorial there were some differences/changes I made to fix issues I had*

## Installing Rosetta
First install Rosetta if not already done, this is needed to run x86 code: 
*Note:Not sure if this is necessary since VMware Fusion 13 supposedly supports M1/M2 Macs*
```
/usr/sbin/softwareupdate --install-rosetta --agree-to-license

```

![Successful Output](https://drive.google.com/file/d/1zDEnzsNRT9Kx82b4162rmrnqrin9K-Rr/view?usp=sharing)

## Installing Vagrant

Install Vagrant via brew or install it manually.
```
brew install vagrant
```
OR
```
brew install vagrant@2.3.3
```

*This worked on Vagrant 2.3.4 on my M1 MBP*

## Installing VMWare Fusion Player
**Step 1:** Register for VMWare Customer Connect (https://customerconnect.vmware.com/account-registration)
*Note: Make sure that you sign up for an individual account, if this option is not available see issues I faced section below.*

![Successful Output](https://drive.google.com/file/d/1obrQYROo1_I1pnEFIE91EdkDwEQ5Bg5i/view?usp=sharing)

**Step 2:** Login into your Customer Connect account and then register for a VMware Fusion Player 13 – Personal Use License from this link (https://customerconnect.vmware.com/evalcenter?p=fusion-player-personal-13)

**Step 3:** Manually download the VMware Fusion Player Installer binary from the licenses and downloads section below. Once downloaded open the folder and follow the instructions from the installer.

![Successful Output](https://drive.google.com/file/d/1AGHuVwOe6_LXMmqQ_VCWehTMRu85DDts/view)

**Step 4:** When asked for the license key, go back to the page where you downloaded the installer and copy the License Key above. 
<Insert pic of license key (Blur this) and download after registering this should appear under licenses and downloads after registering, highlight license key>

![Successful Output](https://drive.google.com/file/d/1h9L5tNkDKtB4MEgSNrwst8jVEIrYHYpk/view?usp=sharing)

### Issues I faced:

**No option to set account as an individual account**

*Try reaching the register page through different ways, for some reason there's multiple versions of this page. Try this for any issues related to registering. One other way is to skip the registration and go directly to step 2, when trying to register for a personal use license it may force you to create a new account.*



## Installing Vagrant VMWare provider

It requires two steps. This is detailed in the [documentation](https://www.vagrantup.com/docs/providers/vmware/installation) but follow the steps below:

It is now available via brew, so you can do

```
brew install --cask vagrant-vmware-utility
```

Otherwise, go to [Vagrant vmware Utility](https://www.vagrantup.com/vmware/downloads) and download the binary and install it. The direct link is [vagrant-vmware-utility_1.0.21_x86_64.dmg](https://releases.hashicorp.com/vagrant-vmware-utility/1.0.21/vagrant-vmware-utility_1.0.21_x86_64.dmg).


It needs to be version 1.0.21

Next install the provider:

```
vagrant plugin install vagrant-vmware-desktop
```

*Note: This step from the original tutorial worked perfectly for me, simply run the 2 commands provided.*

## Create a Vagrant file 

You can use the following example as a box based on Ubuntu 20.04.5:
This is the most basic version of a VagrantFile that worked for me.

You can use any box you want, but make sure that the box is arm compatiablea and supported by vmware. some examples are listed below.

-starboard/ubuntu-arm64-20.04.5
-wilsondc5/centos-9-arm
-uwbbi/bionic-arm64 

```
Vagrant.configure("2") do |config|
    config.vm.box = "starboard/ubuntu-arm64-20.04.5"
    config.vm.provider "vmware_desktop" do |v|
        v.gui = true
        v.vmx["ethernet0.virtualdev"] = "vmxnet3"
    end
end
```

The line ` v.gui = true` is extremely important the vm does not boot up without a gui for some reason.

The line `v.vmx["ethernet0.virtualdev"] = "vmxnet3"` is extremely important otherwise your box will most likely fail to boot as it will try to use the legacy `e1000` VMWare NIC which is apparently not working anymore.

## Run vagrant


```
vagrant up
```

and then

```
vagrant ssh
```
*Note: If prompted for authentication, try username and password to both be "vagrant"*

Hopefully this should work and you should find yourself with everything working.

