#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

int main() {
    int receiverSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (receiverSocket < 0) {
        std::cerr << "Error creating socket.\n";
        return 1;
    }

    sockaddr_in receiverAddr;
    memset(&receiverAddr, 0, sizeof(receiverAddr));
    receiverAddr.sin_family = AF_INET;
    receiverAddr.sin_port = htons(7545);
    receiverAddr.sin_addr.s_addr = INADDR_ANY;

    if (bind(receiverSocket, (struct sockaddr*)&receiverAddr, sizeof(receiverAddr)) < 0) {
        std::cerr << "Error binding socket.\n";
        return 1;
    }

    sockaddr_in senderAddr;
    socklen_t senderAddrLen = sizeof(senderAddr);
    char buffer[1024];

    for (int i = 0; i < 10; ++i) { // Wait for and respond to 10 packets
        double timestamp;
        recvfrom(receiverSocket, &timestamp, sizeof(timestamp), 0, (struct sockaddr*)&senderAddr, &senderAddrLen);
        double responseTime = static_cast<double>(time(0));
        sendto(receiverSocket, &responseTime, sizeof(responseTime), 0, (struct sockaddr*)&senderAddr, senderAddrLen);
    }

    close(receiverSocket);
    return 0;
}
