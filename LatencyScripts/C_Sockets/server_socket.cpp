#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

int main() {
    int senderSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (senderSocket < 0) {
        std::cerr << "Error creating socket.\n";
        return 1;
    }

    sockaddr_in receiverAddr;
    memset(&receiverAddr, 0, sizeof(receiverAddr));
    receiverAddr.sin_family = AF_INET;
    receiverAddr.sin_port = htons(<receiver_port>);
    inet_pton(AF_INET, "<receiver_ip>", &receiverAddr.sin_addr); // ADD THE IP ADDRESS HERE

    for (int i = 0; i < 10; ++i) { // Repeat 10 times for better measurement
        double timestamp = static_cast<double>(time(0));
        sendto(senderSocket, &timestamp, sizeof(timestamp), 0, (struct sockaddr*)&receiverAddr, sizeof(receiverAddr));
        sleep(1); // Adjust the delay between sending packets
    }

    close(senderSocket);
    return 0;
}
