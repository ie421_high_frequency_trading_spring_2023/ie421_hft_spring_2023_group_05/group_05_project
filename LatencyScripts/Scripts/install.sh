#!/bin/bash
# sudo yum -y install git gcc pciutils
apt install git gcc pciutils

mkdir -p /home/vagrant/dev
cd /home/vagrant/dev

# remove the dir if it already exists (in the case that we are rerunning the script)
rm -Rf /home/vagrant/dev/cns-sfnettest
git clone https://github.com/Xilinx-CNS/cns-sfnettest
cd cns-sfnettest/src
make

#copy sfnt-stream applications to ~/bin
ls
mkdir -p /home/vagrant/bin
cp /home/vagrant/dev/cns-sfnettest/src/sfnt-pingpong /home/vagrant/bin

chown -R vagrant:vagrant /home/vagrant/dev
# chmod for root user?